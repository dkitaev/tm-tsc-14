package ru.tsc.kitaev.tm.controller;

import ru.tsc.kitaev.tm.api.controller.IProjectTaskController;
import ru.tsc.kitaev.tm.api.service.IProjectService;
import ru.tsc.kitaev.tm.api.service.IProjectTaskService;
import ru.tsc.kitaev.tm.api.service.ITaskService;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private IProjectTaskService projectTaskService;

    private ITaskService taskService;

    private IProjectService projectService;

    public ProjectTaskController(
            final IProjectTaskService projectTaskService,
            final ITaskService taskService,
            final IProjectService projectService
    ) {
        this.projectTaskService = projectTaskService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    public void bindTaskToProjectById() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (taskService.findById(taskId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        final Task taskUpdated = projectTaskService.bindTaskById(projectId, taskId);
        if (taskUpdated == null) System.out.println("Incorrect values");
    }

    @Override
    public void unbindTaskFromProjectById() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (taskService.findById(taskId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        final Task taskUpdated = projectTaskService.unbindTaskById(projectId, taskId);
        if (taskUpdated == null) System.out.println("Incorrect values");
    }

    @Override
    public void removeAllTaskByProjectId() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        final Project taskUpdated = projectTaskService.removeById(projectId);
        if (taskUpdated == null) System.out.println("Incorrect values");
    }

    @Override
    public void findAllTaskByProjectId() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        final List<Task> taskUpdated = projectTaskService.findTaskByProjectId(projectId);
        if (taskUpdated == null) System.out.println("Incorrect values");
        System.out.println(taskUpdated);
    }

}
