package ru.tsc.kitaev.tm.api.repository;

import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(int index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(int index);

    boolean existsById(String id);

    boolean existsByIndex(int index);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    Task findByProjectIdAndTaskId(String projectId, String taskId);

    List<Task> findAllTaskByProjectId(String projectId);

    Task bindTaskToProjectById(String projectId, String taskId);

    Task unbindTaskById(String projectId, String taskId);

    void removeAllTaskByProjectId(String projectId);

}
